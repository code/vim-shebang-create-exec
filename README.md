shebang\_create\_exec.vim
=========================

This plugin sets up a `BufWritePre` hook to check whether a buffer contains a
Unix shebang with an absolute pathname like `#!/bin/sh` as the first line, and
is being saved to a new file.  If so, it attempts to make the file executable
using the safest method available to Vim.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
